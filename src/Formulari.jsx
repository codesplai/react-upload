import React from "react";
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Row, Col } from 'reactstrap';
import axios from 'axios';



export default class Formulari extends React.Component {
    state={
        nota: '',
        imagen: ''
    }

  constructor(props) {
    super(props);
    //...

   
    this.handleInputChange = this.handleInputChange.bind(this);
    this.onChangeHandler = this.onChangeHandler.bind(this);
    this.submit = this.submit.bind(this);


  }


  submit(e) {
    e.preventDefault();

    let nota = this.state.nota;
    console.log("guardando nota: "+nota);
    //fetch a base de datos...

    const data = new FormData()
   
    data.append('file', this.state.selectedFile);

    axios.post("http://192.168.1.10:8080/upload", data)
      .then(res => { 
        if (res.status==200){
            console.log("OK");
            console.log(res.data.filename);
            this.setState({imagen: res.data.filename });
        }else{
            console.log("algo falla")
        }
      })

  
}

  
  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({
        [name]: value
    });
}


onChangeHandler(event){
    this.setState({
      selectedFile: event.target.files[0],
      loaded: 0,
    })
  }



  render() {

    return (<>
    
    <Form onSubmit={this.submit}>
                    <Row>
                        <Col><h1>Test</h1></Col>
                        <Col><Button type="submit" className='float-right' size='sm' color="primary" >{"Provar"}</Button></Col>
                    </Row>

                    <Row>
                        <Col>
                            <FormGroup>
                                <Label for="notainput">Email</Label>
                                <Input type="text" name="nota" id="notainput"
                                    value={this.state.nota}
                                    onChange={this.handleInputChange} />
                            </FormGroup>
                        </Col>

                        <Col>
                            <FormGroup>
                            <input type="file" name="file" onChange={this.onChangeHandler}/>
                            </FormGroup>
                        </Col>
                    </Row>
                    <Row>

                    <Col>
                            {(this.state.imagen) ?
                            <img src={"http://192.168.1.10:8080/uploads/"+this.state.imagen} />
                            : <></>
                            }
                        </Col>
                    </Row>

                </Form>
    </>);
  }
}
